# Repositorio para publicar presentaciones

A continuación se adjuntan enlaces públicos a diversas presentaciones en formato web (mayoritariamente sobre reveal.js) y en PDF realizadas por mi, Miguel Sevilla-Callejo, y que se encuentran, si no se especifica lo contrario, bajo licencia [Creative Commons Atribución Compartir Igual](https://creativecommons.org/licenses/by-sa/4.0/deed.es_ES).

<!-- Máster UNIZAR 2020 -->

## Máster en Tecnologías de la Información Geográfica

Las presentaciones y el material pertenecen al curso 2019-2020 del [Máster Universitario en Tecnologías de la Información Geográfica para la Ordenación del Territorio: Sistemas de Información Geográfica y Teledetección](https://estudios.unizar.es/estudio/ver?id=608) del Departamento de [Geografía y Ordenación del territorio de la Universidad de Zaragoza](https://geografia.unizar.es).

<!-- módulo QGIS -->

### Módulo de introducción a los SIG libres

Asignatura 60402. _Análisis de la información geográfica: SIG_

Módulo 3.5 / Curso 2019-20

Módulo de introducción a los sistemas de información geográfica sobre programas libres y de código abierto, específicamente, con QGIS.


<img src="https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/slide_intro_sig_libres.png" style="width:250px">


#### Presentaciones

Enlaces a las presentaciones en formato PDF

0. [Introducción a los SIG libres](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/0_introduccion_a_los_sig_libres.pdf)
1. [Presentación a QGIS 3.x](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/01_qgis_presentacion.pdf)
2. [Procesamiento simple y creación cartográfica](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/02_qgis_procesamiento_simple_y_cartografía.pdf)
3. [Procesamiento avanzado, plug-ins y más](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/03_qgis_procesamiento_avanzado.pdf)

#### Ejercicios y otros archivos

- [Ejercicios a entregar](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/ejercicios_mod35_intro_qgis_2020.pdf)
- [Archivos del ejercicio 1](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/ejercicio1.7z)
- [Archivos del ejercicio 2](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/ejercicio2.7z)
- [Ejemplo de proyecto de QGIS con un atlas de las Comarcas de Aragón](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/atlas_comarcas.zip)
- Datos de ejemplo: [Epidemia de cólera de Londres de 1854](https://en.wikipedia.org/wiki/1854_Broad_Street_cholera_outbreak): [snowgis_gpkg.7z](https://msevilla00.gitlab.io/slides/2019-12_mtig35_qgis/snowgis_gpkg.7z)

<!-- módulo Python -->

### Módulo de scripting para el análisis espacial: introducción a Python

Asignatura 60402. _Análisis de la información geográfica: SIG_

Módulo 3.6a / Curso 2019-20

Módulo de introducción a la consola del sistema, los aspectos más básicos de la programación y específicamente, primeros pasos con Python y su aplicación al análisis de datos espaciales. 

<img src="https://msevilla00.gitlab.io/slides/2020-01_mtig36a_python/slide_intro_pyqgis.png" style="width:250px">

<!-- #### Presentaciones -->

0. [_Scripting_](https://msevilla00.gitlab.io/slides/2020-01_mtig36a_python/00_scripting.html)
1. [Presentación de Python](https://msevilla00.gitlab.io/slides/2020-01_mtig36a_python/01_intro_python.html)
2. [Datos y Python](https://msevilla00.gitlab.io/slides/2020-01_mtig36a_python/02_datos_python.html)
3. [Objetos en Python](https://msevilla00.gitlab.io/slides/2020-01_mtig36a_python/03_objetos_python.htm)
4. [Intro a PyQGIS](https://msevilla00.gitlab.io/slides/2020-01_mtig36a_python/04_pyqgis_intro.html)
5. [Más sobre Python](https://msevilla00.gitlab.io/slides/2020-01_mtig36a_python/05_mas_python.html)

<!-- INCLUIR MATERIALES -->



<!-- módulo R -->

### Módulo Usando R para el análisis espacial

Asignatura 60402. Análisis de la información geográfica: SIG

Módulo 3.6b / Curso 2019-20

Módulo de introducción al lenguaje R, al manejo de RStudio y a la composición de mapas sencillos con esta herramienta.

<img src="https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/slide_intro_r.png" style="width:250px">


#### Presentaciones


1. [Presentación de R](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/01_r_intro.html)
2. [_Scripting_ y _data frames_](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/02_r_scripting_dataframes.html)
3. [_dplyr_ y gráficos](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/03_r_dplyr_graficos.html)
4. [Introducción a R espacial](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/04_r_intro_espacial.html)

#### Materiales

- Archivo con **información vectorial** de ejemplo en formato [GeoPackage](https://en.wikipedia.org/wiki/GeoPackage): [vectors.gpkg](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/vectors.gpkg)
- Archivo con **información raster** de ejemplo en formato [GeoTIFF](https://en.wikipedia.org/wiki/GeoTIFF): [raster.tif](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/raster.tif)
- **Tabla ejemplo** de datos internacionales en formato CSV: [gapminder_data.csv](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/gapminder_data.csv)
- Planteamiento del **ejercicio de evaluación** del módulo de R del máster: [ejercicios_r_master_tig_2020.pdf](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/ejercicios_r_master_tig_2020.pdf)
- Indicadores de desarrollo humano de 2018 en formato Excel (copia de descarga de web UNDP): [2018_all_indicators.xlsx](https://msevilla00.gitlab.io/slides/2020-02_mtig36b_r/2018_all_indicators.xlsx)


<!-- módulo visualización online -->

### Módulo Herramientas para la visualización online de la información geográfica

Asignatura 60418. Visualización, presentación y difusión de la información geográfica.

Módulo 5.3 / Curso 2019-20

Módulo de introducción a la representación de información online; primeros pasos con HTML, iniciativas de datos abiertos en la red, sistemas cliente-servidor de datos espaciales y composición básica de mapas online con leaflet.

<img src="https://msevilla00.gitlab.io/slides/2020-04_mtig53_visonline/slide_intro_leaflet.png" style="width:250px">

<!-- #### Presentaciones -->

1. [Introducción HTML](https://msevilla00.gitlab.io/slides/2020-04_mtig53_visonline/01_intro_web.html)
2. Introducción a OSM (pendiente de incluir enlace a material)
3. [Introducción a PostGIS](https://msevilla00.gitlab.io/slides/2020-04_mtig53_visonline/03_intro_postgis.html)
4. [Montando mapas con Leaflet](https://msevilla00.gitlab.io/slides/2020-04_mtig53_visonline/04_intro_leaflet.html)


<!-- INCLUIR MATERIALES -->


## Otras presentaciones

__En construcción__

<!-- para incluir como HTML en el URL público: msevilla00.gitlab.io/slides

Transformar este archivo `README.md` a `./public/index.html` de este repositorio:

pandoc README.md -o ./public/index.html --metadata pagetitle="Presentaciones" --include-in-header=extra_head.html

-->