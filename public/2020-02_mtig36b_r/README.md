Presentaciones y materiales del Módulo 3.6b: _Usando R para el análisis espacial_

[Máster Universitario en Tecnologías de la Información Geográfica para la Ordenación del Territorio: Sistemas de Información Geográfica y Teledetección](https://estudios.unizar.es/estudio/ver?id=608)

Asignatura 60402. Análisis de la información geográfica: SIG
Módulo 3.6b / Curso 2019-20

Profesor: **Miguel Sevilla-Callejo**


- - -
Materiales bajo licencia [Creative Commons BY-SA](https://creativecommons.org/licenses/by-sa/4.0/)